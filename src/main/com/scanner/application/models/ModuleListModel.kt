package com.scanner.application.models

import com.scanner.core.models.ModuleConfigModel
import java.io.File

class ModuleListModel (
    val file: File,
    val data:ModuleConfigModel
)