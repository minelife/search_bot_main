package com.scanner.application

import javafx.application.Application

object Main {

    @JvmStatic
    fun main(vararg args: String) {
        try {
            Application.launch(MainApplication::class.java)
        }catch (e: Exception){e.printStackTrace()}
    }

}