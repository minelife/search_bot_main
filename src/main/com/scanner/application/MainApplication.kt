package com.scanner.application

import com.scanner.application.pages.ModulesLoaderPage
import com.scanner.core.ui.UIApplication
import javafx.stage.Stage

class MainApplication: UIApplication() {

    override fun start(p0: Stage?) {
        ModulesLoaderPage().start()
    }

}