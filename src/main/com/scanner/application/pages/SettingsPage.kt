package com.scanner.application.pages

import com.scanner.core.ui.pages.UIPage
import javafx.stage.Stage

class SettingsPage : UIPage() {

    override fun getPageSize() = Pair(400.0, 750.0)

    override fun start() {
        loadXml("/views/settings_page.fxml", Stage())
    }

}