package com.scanner.application.pages

import com.scanner.core.ui.pages.UIPage
import javafx.stage.Stage

class MenuPage : UIPage() {

    override fun getPageSize() = Pair(400.0, 750.0)

    override fun start() {
        loadXml("/views/menu_page.fxml", Stage())
    }

}