package com.scanner.application.pages

import com.scanner.application.models.ModuleListModel
import com.scanner.core.ui.pages.UIPage
import javafx.stage.Stage

class ModulesLoaderPage : UIPage() {

    companion object{
        var modulesList = ArrayList<ModuleListModel>()
    }

    override fun getPageSize() = Pair(250.0, 350.0)

    override fun start() {
        loadXml("/views/module_loader.fxml", Stage())
    }


}