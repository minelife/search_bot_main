package com.scanner.application.controllers

import com.scanner.application.models.ModuleListModel
import com.scanner.application.pages.MenuPage
import com.scanner.application.pages.ModulesLoaderPage
import com.scanner.core.ui.UIController
import com.scanner.core.utils.GlobalParamsProvider
import com.scanner.core.utils.JarLoader
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.scene.control.Label
import java.io.File
import java.net.URL
import java.util.*

class ModuleLoaderController:UIController() {

    @FXML
    lateinit var loaderText: Label

    override fun initialize(p0: URL?, p1: ResourceBundle?) {
        Thread {
            readModules()
        }.start()
    }

    private fun readModules(){
        val f = File("modules")
        if(!f.exists())f.mkdir()
        f.listFiles()?.forEach {
            if(it.extension != "jar")return@forEach
            Platform.runLater{
                loaderText.text = "Reading file: ${it.path}"
            }
            val m = JarLoader.loadModuleConfig(it)
            if (m!= null) {
                ModulesLoaderPage.modulesList.add(ModuleListModel(it, m))
                if(m.params!=null)
                    GlobalParamsProvider().forceUpdateParams(m.params!!)
                Platform.runLater{
                    loaderText.text = "Module: ${m.name} successfully added!"
                }
            }else {
                Platform.runLater {
                    loaderText.text = "File: ${it.path} don't have config file!"
                }
            }


        }

        Platform.runLater{
            hideThisPage(loaderText)
            MenuPage().start()
        }
    }

}