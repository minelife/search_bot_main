package com.scanner.application.controllers

import com.scanner.application.models.ModuleListModel
import com.scanner.application.pages.ModulesLoaderPage
import com.scanner.application.pages.SettingsPage
import com.scanner.core.ui.UIController
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.system.exitProcess

class MenuPageController: UIController() {

    private val builtMenu = HashMap<String, Map<String, List<ModuleListModel>>>()
    private var breadCrumbs = ArrayList<String>()

    @FXML
    lateinit var menuItems:VBox
    @FXML
    lateinit var settings:Button
    @FXML
    lateinit var exit:Button

    override fun initialize(p0: URL?, p1: ResourceBundle?) {
        Thread {
            ModulesLoaderPage.modulesList.groupBy { it.data.menuPath.split("/")[0] }.forEach { (pKey, pRes) ->
                builtMenu[pKey] = pRes.groupBy {it.data.menuPath.split("/")[1]}
            }
            buildMenu(0)
        }.start()

        settings.setOnMouseClicked {
            hideThisPage(settings)
            SettingsPage().start()
        }

        exit.setOnMouseClicked {
            exitProcess(0)
        }

    }

    private fun buildMenu(level: Int){
        Thread{
            when (level) {
                0 -> {
                    breadCrumbs.clear()
                    clearMenu()
                    buildFromList(level, builtMenu.keys.toList())
                }
                1 -> {
                    clearMenu()
                    addBackButton(0)
                    buildFromList(level, builtMenu[breadCrumbs[0]]!!.keys.toList())
                }
                2 -> {
                    clearMenu()
                    addBackButton(1)
                    buildFromList(level, builtMenu[breadCrumbs[0]]!![breadCrumbs[1]]!!.map { it.data.name }.toList())
                }

                else -> {
                    val m = builtMenu[breadCrumbs[0]]!![breadCrumbs[1]]!!.filter { it.data.name == breadCrumbs[2] }.first()
                    Platform.runLater {
                        openPageFromJar(m.file, m.data.startPage)
                    }
                }
            }

        }.start()
    }

    private fun clearMenu(){
        Platform.runLater {
            menuItems.children.clear()
        }
    }

    private fun getHBox(node: Node):HBox{
        val h = HBox()
        h.alignment = Pos.BASELINE_CENTER
        h.isFillHeight = true
        h.children.add(node)
        return h
    }

    private fun addBackButton(l: Int){
        val h = getHBox(
            addMenuButton("<- Back") {
                buildMenu(l)
            }
        )
        Platform.runLater {
            menuItems.children.add(h)
        }
    }

    private fun buildFromList(level: Int, list: List<String>){

        list.forEach {
            val h = getHBox(
                addMenuButton(it) {
                    if(breadCrumbs.size<=level)
                        breadCrumbs.add(level, it)
                    else
                        breadCrumbs[level] = it
                    buildMenu(level + 1)
                }
            )
            Platform.runLater {
                menuItems.children.add(h)
            }
        }

    }

    private fun addMenuButton(text: String, click: () -> Unit):Button{
        val b = Button(text)
        b.styleClass.add("menu_button")
        b.prefWidth = 200.0
        HBox.setMargin(b, Insets(4.0, 0.0, 4.0, 0.0))
        b.setOnMouseClicked{
            click()
        }
        return b
    }

}