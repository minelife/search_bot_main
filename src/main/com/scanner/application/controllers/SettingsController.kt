package com.scanner.application.controllers

import com.scanner.application.pages.MenuPage
import com.scanner.core.ui.controllers.UISettingsPageController
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.layout.VBox
import java.net.URL
import java.util.*

class SettingsController : UISettingsPageController() {
    
    @FXML
    lateinit var back:Button 
    @FXML
    lateinit var save:Button
    @FXML
    lateinit var settingsRoot:VBox
    
    override fun getRootNodeForParams(): VBox = settingsRoot

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        super.initialize(location, resources)
        back.setOnMouseClicked{
            hideThisPage(save)
            MenuPage().start()
        }
        save.setOnMouseClicked{
            saveSettings()
        }
    }
}